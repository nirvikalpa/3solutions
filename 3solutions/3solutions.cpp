﻿//==========================================================================================================
// Sidelnikov Dmitry
// Решения трех задач компании ХХХ
// 07.07.2020
// Все решения заняли окло 5 часов
//==========================================================================================================

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <unordered_map>
#include <string.h>

//==========================================================================================================
// TASK 1 - конвертирование числа в бинарное представление
//==========================================================================================================
template <class T>
void PrintBin(T num)
{
    const size_t bufsize = sizeof(num) * 8;
    char* buf = new (std::nothrow) char[bufsize + 1];
    if (buf != nullptr)
    {
        buf[bufsize] = 0;
        int mask = 1;
        int index = bufsize - 1;

        for (size_t i = 0; i < bufsize; i++)
        {
            if (num & mask)
                buf[index] = '1';
            else
                buf[index] = '0';

            mask <<= 1;
            index--;
        }

        std::cout << buf << std::endl;
        delete[] buf;
    }
    else
    {
        std::cerr << "Error! Can not allocate memory." << std::endl;
    }
}

//==========================================================================================================
// TASK 2 - удаление дубликатов в строке
//==========================================================================================================
void RemoveDup(char* str)
{
    const size_t strLen = strlen(str);
    size_t rIndex = 1; // read index
    size_t wIndex = 0; // write index

    do {

        if (str[rIndex] != str[wIndex])
        {
            if (rIndex - wIndex == 1)
            {
                wIndex++;
            }
            else
            {
                str[++wIndex] = str[rIndex];
            }
        }

        rIndex++;

    } while (rIndex != strLen);

    str[++wIndex] = 0;
}

//==========================================================================================================
// TASK 3 - написать функции сериализации и десереализации для данной структуры
//==========================================================================================================

// структуру ListNode модифицровать нельзя
struct ListNode {
    ListNode() : prev(nullptr), next(nullptr), rand(nullptr) { }

    ListNode* prev;
    ListNode* next;
    ListNode* rand; // указатель на произвольный элемент данного списка, либо NULL

    std::string data;
};

class List {
public:
    List() { head = nullptr; tail = nullptr; count = 0; }
    ~List() { Clear(); }

    void Serialize(FILE* file);   // сохранение в файл (файл открыт с помощью fopen(path, "wb"))
    void Deserialize(FILE* file); // загрузка из файла (файл открыт с помощью fopen(path, "rb"))

    void PushBack(std::string data);
    void Clear();
    void Print();

    ListNode* operator[](size_t index);

private:
    ListNode* head;
    ListNode* tail;
    size_t count;
};

void List::PushBack(std::string data)
{
    ListNode* newNode = new (std::nothrow) ListNode;
    if (newNode != nullptr)
    {
        newNode->next = nullptr;
        newNode->prev = tail;
        newNode->rand = nullptr;
        newNode->data = data;
        if (tail != nullptr)
        {
            tail->next = newNode;
        }
        else
        {
            head = newNode;
        }

        tail = newNode;
        count++;
    }
}

void List::Clear()
{
    while (head != nullptr)
    {
        ListNode* temp = head->next;
        delete head;
        head = temp;
    }
    head = nullptr;
    tail = nullptr;
    count = 0;
}

void List::Print()
{
    ListNode* node = head;
    size_t i = 0;
    while (node != nullptr)
    {
        std::cout << "[" << i << "] addr: " << node << ", data: " << node->data << ", randPtr: " << node->rand << std::endl;
        node = node->next;
        i++;
    }

    if (i == 0)
    {
        std::cout << "List is empty!" << std::endl;
    }
}

ListNode* List::operator[](size_t index)
{
    if (index < count)
    {
        ListNode* node = head;
        while ((node != nullptr) && (index > 0))
        {
            node = node->next;
            index--;
        }
        return node;
    }
    else
    {
        std::cerr << "Error! Incorrect index: " << index << std::endl;
    }
    return nullptr;
}

void List::Serialize(FILE* file) // сохранение в файл (файл открыт с помощью fopen(path, "wb"))
{
    if ((count == 0) || (head == nullptr))
    {
        std::cerr << "Error! List must NOT be empty for List::Serialize(...)" << std::endl;
        return;
    }

    char buf1[] = "LISTDATA";
    std::fwrite(buf1, 8, 1, file); // Заголовок файла для уникальности

    std::fwrite(&count, sizeof(count), 1, file); // Количество элементов в списке

    ListNode* node = head;
    std::unordered_map<ListNode*, int> map;
    int i = 0;
    while (node != nullptr)
    {
        map[node] = i++;
        node = node->next;
    }

    node = head;
    int rand;
    while (node != nullptr)
    {
        if (node->rand != nullptr)
        {
            rand = map[node->rand];
        }
        else
        {
            rand = -1;
        }
        std::fwrite(&rand, sizeof(rand), 1, file); // Сохраняем индекс эелемента на который ссылается поле rand

        // Сохраняем строку data и ее размер
        const size_t size = node->data.size();
        std::fwrite(&size, sizeof(size), 1, file);
        std::fwrite(node->data.c_str(), size, 1, file);
        node = node->next;
    }
}

void List::Deserialize(FILE* file) // загрузка из файла (файл открыт с помощью fopen(path, "rb"))
{
    if ((count != 0) || (head != nullptr))
    {
        std::cerr << "Error! List must be empty for List::Deserialize(...)" << std::endl;
        return;
    }

    char buf1[9];
    std::fread(buf1, 8, 1, file);
    buf1[8] = 0;
    if (strcmp(buf1, "LISTDATA") != 0)
    {
        return;
    }

    int newCount = 0;
    std::fread(&newCount, sizeof(newCount), 1, file);

    char* buf;
    size_t size;
    int* randArray = new (std::nothrow) int[newCount];
    ListNode** ppListElemAddr = new (std::nothrow) ListNode * [newCount];
    if ((randArray != nullptr) && (ppListElemAddr != nullptr))
    {

        for (int i = 0; i < newCount; i++)
        {
            std::fread(&randArray[i], sizeof(int), 1, file);

            std::fread(&size, sizeof(size), 1, file);

            buf = new (std::nothrow) char[size + 1];
            if (buf != nullptr)
            {
                std::fread(buf, size, 1, file);
                buf[size] = 0;
                PushBack(std::string(buf));
                delete[] buf;
                buf = nullptr;

                ppListElemAddr[i] = tail;
            }
        }

        int i = 0;
        ListNode* node = head;
        while (node != nullptr)
        {
            if (randArray[i] == -1)
            {
                node->rand = nullptr;
            }
            else
            {
                if (randArray[i] < newCount)
                {
                    node->rand = ppListElemAddr[randArray[i]];
                }
            }
            node = node->next;
            i++;
        }

        delete[] randArray;
        delete[] ppListElemAddr;
    }
}

//==========================================================================================================
// MAIN
//==========================================================================================================
int main()
{
    // TASK 1 ----------------------------------------------------------------------------------------------

    PrintBin<long long>(255); // 0000000000000000000000000000000000000000000000000000000011111111
    PrintBin<int>(LONG_MAX);  // 01111111111111111111111111111111
    PrintBin<int>(1024);      // 00000000000000000000010000000000
    PrintBin<int>(64);        // 00000000000000000000000001000000
    PrintBin<short int>(1);   // 0000000000000001
    PrintBin<short int>(0);   // 0000000000000000

    // TASK 2 ----------------------------------------------------------------------------------------------

    std::cout << std::endl;
    char str1[] = "AAA BBBB CC D E FFFFF 111111123455555\0";
    std::cout << str1 << std::endl;
    RemoveDup(str1);
    std::cout << str1 << std::endl << std::endl; // A B C D E F 12345

    // TASK 3 ----------------------------------------------------------------------------------------------

    List list1;
    list1.PushBack(std::string("Test 11"));
    list1.PushBack(std::string("Test 2"));
    list1.PushBack(std::string("Test 333"));
    list1.PushBack(std::string("Test 4"));
    list1.PushBack(std::string("Test 55"));
    list1[0]->rand = list1[3];
    list1[4]->rand = list1[1];
    list1.Print();

    const char filename[] = "1.bin";
    std::FILE* file = std::fopen(filename, "wb");
    if (file != nullptr)
    {
        list1.Serialize(file); // сохранение в файл
        fclose(file);

        list1.Clear(); // очистка списка
        std::cout << "List is cleared." << std::endl;

        file = std::fopen(filename, "rb");
        if (file != nullptr)
        {
            list1.Deserialize(file); // чтение из файла
            fclose(file);
            list1.Print();
        }
    }

    // [0] addr: 00000226D121FA50, data : Test 11, randPtr : 00000226D1214E10
    // [1] addr : 00000226D12150C0, data : Test 2, randPtr : 0000000000000000
    // [2] addr : 00000226D1215370, data : Test 333, randPtr : 0000000000000000
    // [3] addr : 00000226D1214E10, data : Test 4, randPtr : 0000000000000000
    // [4] addr : 00000226D1213520, data : Test 55, randPtr : 00000226D12150C0
    // List is cleared.
    // [0] addr : 00000226D1214E10, data : Test 11, randPtr : 00000226D12150C0
    // [1] addr : 00000226D1213520, data : Test 2, randPtr : 0000000000000000
    // [2] addr : 00000226D1215370, data : Test 333, randPtr : 0000000000000000
    // [3] addr : 00000226D12150C0, data : Test 4, randPtr : 0000000000000000
    // [4] addr : 00000226D1222B40, data : Test 55, randPtr : 00000226D1213520

    return 0;
}

